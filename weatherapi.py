from urllib import request
import os
import json

# https://openweathermap.org/current
class WeatherAPI():
    def __init__(self, configuration):
        self.configuration = configuration
        self.unit = configuration["unit"]
        self.language = configuration["language"]
        self.apikey = configuration["key"]
        # Fetch API key from environment variable if indicated
        if self.apikey[0] == "$":
          self.apikey = os.environ[self.apikey[1:]]

    def getWeather(self, city, country=""):
        query = ",".join([city, country])
        url = 'http://api.openweathermap.org/data/2.5/weather?q={0}&appid={1}&units={2}&lang={3}'.format(
            query, self.apikey, self.unit, self.language)
        response = request.urlopen(url)
        data=json.loads(response.read())
        # TODO: handle empty response, missing fields etc.
        weather = {
            "unit": self.unit,
            "language": self.language,
            "city": city,
            "country": country,
            "label": data["weather"][0]["description"],
            "icon": data["weather"][0]["icon"],
            "temperature": data["main"]["temp"],
            "pressure": data["main"]["pressure"],
            "humidity": data["main"]["humidity"],
            "clouds": data["clouds"]["all"],
            "wind": {
                "speed": data["wind"]["speed"],
                "direction": data["wind"]["deg"]
            }
        }
        weather["description"] = self.getWeatherString(weather)
        return weather

    def degreesToDirection(self, value):
        directions = ["N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE",
                      "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW"]
        index = int((value + 11.25) / 22.5)
        return directions[index % 16]

    def getLocation(self, city, country):
        return city

    def getTemperatureString(self, value):
        return "{0}°C".format(round(value))

    def getWindString(self, value):
        kmh = value["speed"] * 3.6
        if kmh <= 3:
            return "quiet"
        if kmh <= 5:
            return "gentle breeze"
        if kmh <= 10:
            return "light breeze"
        if kmh <= 15:
            return "breeze"
        if kmh <= 25:
            return "strong breeze"
        if kmh <= 35:
            return "wind"
        if kmh <= 50:
            return "strong wind"
        return "very strong wind"

    def getWeatherString(self, weather):
        label = weather["label"]
        location = self.getLocation(weather["city"], weather["country"])
        temperature = self.getTemperatureString(weather["temperature"])
        wind = self.getWindString(weather["wind"])
        return ", ".join([location, temperature, label, wind])
