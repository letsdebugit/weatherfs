#!/usr/bin/env python

import sys
from passthrough import Passthrough
from fuse import FUSE

def main(mountpoint, root):
    FUSE(Passthrough(root), mountpoint, nothreads=True, foreground=True)


if __name__ == '__main__':
    mountpoint = sys.argv[2]
    root = sys.argv[1]
    main(mountpoint, root)
