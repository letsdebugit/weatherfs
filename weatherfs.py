# Weather FS

# Load weather for specified locations from some open weather API
# Simulate image files in the folder,
# depending on kind of weather in place.
# Overlay weather conditions in file name.

import os
import sys
import random
from errno import ENOENT
from fuse import FUSE, FuseOSError, Operations
from stat import S_IFDIR, S_IFLNK, S_IFREG
from time import time

# All files under WeatherFS are read-only for all users
DEFAULT_FILE_MODE = 0o444
REFRESH_INTERVAL = 5

class WeatherFS(Operations):
    def __init__(self, mountpoint, configuration, weatherAPI):
        self.mountpoint = mountpoint
        self.configuration = configuration
        self.weatherAPI = weatherAPI
        self.fd = 0
        self.uid = os.getuid()
        self.gid = os.getgid()
        self.sourcePath = os.path.dirname(__file__)
        now = time()
        self.files = {}
        self.files['/'] = dict(
            st_mode=(S_IFDIR | 0o555),
            st_ctime=now,
            st_mtime=now,
            st_atime=now,
            st_nlink=2
        )
        self.lastRefreshedAt = None

    # Creates file representing weather for the specified city
    def getWeather(self, city, country):
        weather = self.weatherAPI.getWeather(city, country)
        icon = weather['icon']
        description = weather['description']
        path = '/{0}.png'.format(description)
        self.create(path)
        self.chown(path, self.uid, self.gid)
        self.files[path]['attrs']['weather'] = weather
        iconPath = self.getIconPath(path)
        # TODO: handle situation when icon not found,
        # eg. when OpenWeatherAPI introduces new icons
        iconStats = os.stat(iconPath)
        self.files[path]['st_size'] = iconStats.st_size

    # Refreshes weather for all locations
    def refreshWeather(self):
        now = time()
        diff = REFRESH_INTERVAL if self.lastRefreshedAt is None else now - self.lastRefreshedAt
        if diff < REFRESH_INTERVAL:
            print('NO REFRESH NEEDED')
        else:
            print('REFRESHING ...')
            root = self.files['/']
            self.files = {
                '/': root
            }
            self.getWeather('Dublin', 'IE')
            self.getWeather('Thiers', 'FR')
            self.getWeather('Trapani', 'IT')
            self.lastRefreshedAt = now

    # Returns full path to icon file
    # for the specified weather file
    def getIconPath(self, path):
        try:
            file = self.files[path]
            icon = file['attrs']['weather']['icon']
            iconPath = os.path.join(self.sourcePath, 'images', 'icons', '{0}@4x.png'.format(icon))
            return iconPath
        except:
            return None

    # Filesystem methods
    # ==================
    def readdir(self, path, fh):
        print("WeatherFS.readdir", path)
        self.refreshWeather()
        return ['.', '..'] + [x[1:] for x in self.files if x != '/']

    def getattr(self, path, fh=None):
        print("WeatherFS.getattr", path)
        if path not in self.files:
            raise FuseOSError(ENOENT)
        return self.files[path]

    def getxattr(self, path, name, position=0):
        print('getxattr', path, name)
        if path not in self.files:
            raise FuseOSError(ENOENT)
        try:
            attrs = self.files[path].get('attrs', {})
            return attrs[name]
        except:
            return ''

    def listxattr(self, path):
        print('listxattr', path)
        if path not in self.files:
            raise FuseOSError(ENOENT)
        attrs = self.files[path].get('attrs', {})
        return attrs.keys()

    def statfs(self, path):
        print("WeatherFS.statfs", path)
        return dict(f_bsize=512, f_blocks=4096, f_bavail=2048)

    def utimens(self, path, times=None):
        print("WeatherFS.utimens", path)
        return os.utime(self.mountpoint, times)

    def readlink(self, path):
        print("WeatherFS.readlink", path)
        return path

    def create(self, path, mode=DEFAULT_FILE_MODE, fi=None):
        print("WeatherFS.create", path, mode)
        now = time()

        self.fd += 1
        fh = self.fd

        self.files[path] = dict(
            st_mode=(S_IFREG | mode),
            st_nlink=1,
            st_size=120000,
            st_ctime=now,
            st_mtime=now,
            st_atime=now,
            attrs={
                'fh': fh,
                'icon_fh': None
            })

        return fh

    def chown(self, path, uid, gid):
        print("WeatherFS.chown", path, uid)
        self.files[path]['st_uid'] = self.uid
        self.files[path]['st_gid'] = self.gid

    def open(self, path, flags):
        print("WeatherFS.open", path, flags)
        try:
            iconPath = self.getIconPath(path)
            file = self.files[path]
            file['attrs']['icon_fh'] = os.open(iconPath, flags)
            return file['attrs']['fh']
        except Exception as error:
            print(error)
            return 0

    def read(self, path, length, offset, fh):
        print("WeatherFS.read", path, length, offset)
        try:
            file = self.files[path]
            icon_fh = file['attrs']['icon_fh']
            if icon_fh is not None:
                os.lseek(icon_fh, offset, os.SEEK_SET)
                return os.read(icon_fh, length)
        except Exception as error:
            print(error)
            return 0

    def release(self, path, fh):
        print("WeatherFS.release", path)
        try:
            file = self.files[path]
            icon_fh = file['attrs']['icon_fh']
            if icon_fh is not None:
                return os.close(icon_fh)
        except Exception as error:
            print(error)
            return 0

    # Operations not available on WeatherFS virtual files,
    # therefore implemented as dummy.
    # TODO: is this actually needed??
    # Maybe fusepy already provides dummies??
    def chmod(self, path, mode):
        print("WeatherFS.chmod", path, mode)
        return 0

    def symlink(self, name, target):
        print("WeatherFS.symlink", path)
        return 0

    def rename(self, old, new):
        print("WeatherFS.rename", old, new)
        return 0

    def link(self, target, name):
        print("WeatherFS.link", target, name)
        return 0

    def rmdir(self, path):
        print("WeatherFS.rmdir", path)
        return 0

    def mkdir(self, path, mode):
        print("WeatherFS.mkdir", path, mode)
        return 0

    def unlink(self, path):
        print("WeatherFS.unlink", path)
        return 0

    def mknod(self, path, mode, dev):
        print("WeatherFS.mknod", path, mode)
        return 0

    def write(self, path, buf, offset, fh):
        print("WeatherFS.write", path, buf)
        return 0

    def truncate(self, path, length, fh=None):
        print("WeatherFS.truncate", path, length)
        return 0

    def flush(self, path, fh):
        print("WeatherFS.flush", path)
        return 0

    def fsync(self, path, fdatasync, fh):
        print("WeatherFS.fsync", path)
        return 0
