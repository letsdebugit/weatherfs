# Periodic refresh
To ensure fresh weather when folder remains open, refresh every 15 minutes or so. Is there a way of knowing that the folder is being viewed, in Python? In bash that's `lsof`

# Navigate to weather
Make files executable, open browser with weather details if double clicked.

# Configuration
* Weather states: label, description, icon
* Refresh interval
* API and keys
* Command to execute on dblclick
