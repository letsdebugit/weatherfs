import sys
from fuse import FUSE, FuseOSError, Operations
import json
from weatherfs import WeatherFS
from weatherapi import WeatherAPI

def main(mountpoint):
    with open("configuration.json") as cf:
        configuration = json.load(cf)
    weatherAPI = WeatherAPI(configuration["api"])
    FUSE(
      WeatherFS(mountpoint, configuration, weatherAPI),
      mountpoint,
      nothreads=True,
      foreground=True
    )

if __name__ == '__main__':
    if len(sys.argv) == 2:
        mountpoint = sys.argv[1]
        main(mountpoint)
    else:
        print("Mount point is required")
